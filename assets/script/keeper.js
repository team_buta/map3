// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        map: {
            default: null,
            type: cc.Node
        },
        mapInfo: {
            default: null,
        },
        //それぞれの向きの画像
        front: {
            default: null,
            type: cc.Prefab
        },
        back: {
            default: null,
            type: cc.Prefab
        },
        right: {
            default: null,
            type: cc.Prefab
        },
        left: {
            default: null,
            type: cc.Prefab
        },
        keeperX: 3,
        keeperY: 1,
        lastTimeMove: null,
        actionEnd: null,
        chase: false,
        moveDistance: 0.4,
        elapsedTime: 0,
        queue: null,
        name:"name",
    },

    // LIFE-CYCLE CALLBACKS:

    //これは仮の姿
    move: function() {
        if (this.actionEnd !== null) {
            return;
        }
        const playerX = this.map.getComponent("Map").playerPositionX;
        const playerY = this.map.getComponent("Map").playerPositionY;
        console.log(playerX + "&" + playerY);
        console.log(this.keeperX + "$" + this.keeperY);
        if (playerY < this.keeperY) {
            if (this.lastTimeMove !== "down") {

                this.lastTimeMove = "up";
                this.enqueue("up");
            }
        } else if (playerY > this.keeperY) {
            if (this.lastTimeMove !== "up") {

                this.lastTimeMove = "down";
                this.enqueue("down");
            }
        } else if (playerX > this.keeperX) {
            //ここで移動先に物があるかチェック
            if (this.lastTimeMove !== "left") {

                this.lastTimeMove = "right";
                this.enqueue("right");
            }
        }
        if (playerX < this.keeperX) {
            if (this.lastTimeMove !== "right") {

                this.lastTimeMove = "left";
                this.enqueue("left");
            }
        }
        this.moveAction();
        this.catchCheck();
    },

    moveAction: function() {
        let move;
        switch (this.lastTimeMove) {
            case "up":
                move = cc.moveBy(0.4, cc.v2(0, 48));
                console.log("妖精さん:うえいこ");
                this.keeperY--;
                break;
            case "down":
                move = cc.moveBy(0.4, cc.v2(0, -48));
                console.log("妖精さん:したいこ");
                this.keeperY++;
                break;
            case "right":
                move = cc.moveBy(0.4, cc.v2(48, 0));
                console.log("妖精さん:みぎいこ");
                this.keeperX++;
                break;
            case "left":
                move = cc.moveBy(0.4, cc.v2(-48, 0));
                console.log("妖精さん:ひだりいこ");
                this.keeperX--;
                break;
            default:
                move = cc.rotateBy(5, 5);
                break;
        }

        this.actionEnd = this.node.getChildByName(this.name).runAction(move);
        this.scheduleOnce(function() {
            this.actionEnd = null;
        }, this.moveDuration);
    },

    catchCheck: function() {
        const keeperPos = this.node.getChildByName(this.name).getPosition();
        const playerPos = this.map.getComponent("Map").player.getPosition();
        const distance = playerPos.sub(keeperPos).mag();
        console.log(distance);
        if (distance < 48) {
            console.log("捕まえたﾆﾁｬｧ");
            this.chase = false;
        }
    },

    enqueue: function(data) {
        this.queue.push(data);
    },

    dequeue: function() {
        if (this.queue.length > 0) {
            return this.queue.shift();
        }
        return null;
    },

    followMap: function(vector) {
        let move = cc.moveBy(0.5, vector);
        this.node.getChildByName(this.name).runAction(move);
    },

    chaseStart: function() {
        console.log("chaseStart");
        this.chase = true;
        this.elapsedTime = 0;
    },

    onLoad(playerX, playerY) {
        //マップを取得
        this.mapInfo = this.map.getComponent("Map").info;
        this.queue = new Array();
    },

    // start () {},

    update(dt) {
        if (this.chase) {
            this.elapsedTime += dt;
            if (this.elapsedTime >= this.moveDistance && this.actionEnd === null) {
                this.move();
                this.elapsedTime = 0;
            }
        }
    },
});
