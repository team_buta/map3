// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        player:{
            default:null,
        },
        front:{
            default:null,
            type:cc.Prefab,
        },
        right:{
            default:null,
            type:cc.Prefab,
        },
        left:{
            default:null,
            type:cc.Prefab,
        },
        back:{
            default:null,
            type:cc.Prefab,
        },
        map:{
          default:null,
          type: cc.Node
        },
        span:0,
        vector:null,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.player = cc.instantiate(this.front);
        this.node.addChild(this.player,3,"Player");
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    },


    onKeyDown:function(event){
        if (this.span < 0.5){
            return;
        }
        switch (event.keyCode) {
            case cc.macro.KEY.a:
                this.player = cc.instantiate(this.left);
                this.vector = "left";
                break;
            case cc.macro.KEY.d:
                this.player = cc.instantiate(this.right);
                this.vector = "right";
                break;
            case cc.macro.KEY.w:
                this.player = cc.instantiate(this.back);
                this.vector = "back";
                break;
            case cc.macro.KEY.s:
                this.player = cc.instantiate(this.front);
                this.vector = "front";
                break;
            default:
                return;
                break;
        }
        //今までの自分に別れを告げる
        this.node.getChildByName("Player").destroy();
        //こんにちは。あたらしい自分
        this.node.addChild(this.player,3,"Player");
        console.log("X:" + this.map.getComponent('Map').playerPositionX);
        this.span = 0;
    },
    start () {

    },

    update (dt) {
        this.span += dt;
    },
});
